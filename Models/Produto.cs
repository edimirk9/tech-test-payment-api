using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Item_Produto { get; set; }
        public string Nome_Produto { get; set; }
        
    }
}